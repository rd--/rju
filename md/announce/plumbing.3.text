To: jackit-devel@lists.sourceforge.net linux-audio-announce@music.columbia.edu
Subject: [ANN] jack.plumbing - Announce
From: Rohan Drape <rd@alphalink.com.au>
Date: 14 Mar 2004 12:27:17 +1100

jack.plumbing - Announce

The JACK plumbing daemon has a new rule to dramatically reduce
ordinary rule set sizes, a new system wide configuration file, and a
new version number to indicate progress.

The rule 'also-connect' makes managing machines that connect to a
studio using an ethernet cable simpler.  Adding the entry:

(also-connect "alsa_pcm:playback_1"  "jack.udp-[0-9]*:in_1")

to ~/.jack.plumbing ensures that when jack.udp(1) is running in send
mode all signals that are ordinarily sent to the local audio interface
will also be sent to the network destination.  The also-connect
aliasing applies to both the left and right hand side of connect
rules, so that:

(also-connect "jack.udp-[0-9]*:out_\1" "alsa_pcm:capture_\1")

works as expected.

The changes required to support this rule exacerbate the loss of
efficiency that began with the introduction of sub-expression rules.
Again the reduction is rule set sizes is the compensation, however
JACK sessions with ~= 100 ports provide serious exercise for the
plumber.

The file /etc/jack.plumbing is now consulted if it exists.

A source archive and a copy of the manual page are available at:
<http://www.alphalink.com.au/~rd>

Regards,
Rohan
