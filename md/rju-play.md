RJU-PLAY(1)
============

NAME
----
rju-play - JACK Sound File Player

SYNOPSIS
--------
rju-play [options] sound-file...

OPTIONS
-------
*-b*
:   Set the disk buffer size in frames (default=4096).  This value
    must be a power of two.  If the JACK period size at any time
    exceeds this value the player will halt.

*-c*
:   Set the sample rate conversion algorithm (default=2).  Values are:
    `SRC_SINC_BEST_QUALITY` = 0, `SRC_SINC_MEDIUM_QUALITY` = 1,
    `SRC_SINC_FASTEST` = 2, `SRC_ZERO_ORDER_HOLD` = 3 and `SRC_LINEAR` = 4.

*-d*
:   Set the destination port pattern, overrides environment variable.

*-i*
:   Set the initial disk seek in frames (default=0).

*-l*
:   Loop input file indefinitely.

*-m*
:   Set the minimal disk transfer size in frames (default=32).  This
    is an optimization switch.

*-n*
:   Set the client name (default=rju-play).

*-q*
:   Set the frame size to request data from the ringbuffer
    (default=64). This is an optimization switch.

*-r*
:   Set the resampling ratio multiplier (default=1.0).

*-t*
:   Run in JACK transport aware mode.

*-u*
:   Do not make client name unique by appending process  identifier.

DESCRIPTION
-----------
rju-play is a light-weight JACK sound file player. It creates as many
output ports as there are channels in the input file.  It will connect
to ports mentioned at *-d* or in the environment variable
RJU_PLAY_CONNECT_TO which must include a %d pattern to indicate port
number, otherwise it implements no connection logic, use
rju-plumbing(1) instead.

rju-play will read files in any format supported by libsndfile, and
will resample to match the server sample rate using libsamplerate.

AUTHOR
------
Rohan Drape <http://rd.slavepianos.org/>, November 2003

SEE ALSO
--------
jackd(1), libsndfile(3) <http://mega-nerd.com/libsndfile/>,
libsamplerate(3) <http://mega-nerd.com/SRC/>
