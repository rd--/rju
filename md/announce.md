# announce

- [osc](https://lists.linuxaudio.org/archives/linux-audio-announce/2004-December/000502.html)
- [plumbing.1](https://narkive.com/DbRqr9mN.1)
- [plumbing.2](https://lists.linuxaudio.org/archives/linux-audio-announce/2004-March/000340.html)
- [record](https://narkive.com/IXHwtUR3)
- [scope](https://narkive.com/Mv7EO0Tf)

<!--
- [clock](http://article.gmane.org/gmane.comp.audio.jackit/5726),
- [plumbing](http://article.gmane.org/gmane.comp.audio.jackit/3655)
- [scope](http://article.gmane.org/gmane.comp.audio.jackit/5727)
- [udp](http://article.gmane.org/gmane.comp.audio.jackit/5548)
-->
